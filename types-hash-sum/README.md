# Installation
> `npm install --save @types/hash-sum`

# Summary
This package contains type definitions for hash-sum (https://github.com/bevacqua/hash-sum).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/hash-sum

Additional Details
 * Last updated: Sat, 25 Aug 2018 01:04:12 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Daniel Rosenwasser <https://github.com/DanielRosenwasser>.
